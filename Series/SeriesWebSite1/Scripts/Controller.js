﻿myapp.controller('SeriesController', function ($scope, SeriesService) {
    
    $scope.SeriesLimit = "";
    $scope.Divisibleby = "";
    $scope.Position = "";
    $scope.Message = "Currenty there is no input";

    $scope.FetchSeries = function () {
        
        var promiseGet = SeriesService.GetSeries($scope.SeriesLimit, $scope.Divisibleby, $scope.Position);
        promiseGet.then(function (p) {
           
            $scope.result = p.data.Result;
            $scope.SericesList = p.data.Series;
            $scope.Message = p.data.Message;
        },
              function (errorPl) {
                  $log.error('Some Error in Getting Records.', errorPl);
              });
    }

});
