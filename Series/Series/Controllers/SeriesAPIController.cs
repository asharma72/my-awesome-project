﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Series.Models;

namespace Series.Controllers
{
    
    public class SeriesAPIController : ApiController
    {
        
        ///EndPoint: http://localhost:63175/api/SeriesAPI?SeriesLimit=20&divisible=5&position=3
        /// <summary>
        /// GET: api/Series
        /// </summary>
        /// <param name="divisible"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public ResponseMessage GetSeriesValue(int? SeriesLimit, int? divisible = 0, int? position = 0)
        {
            ResponseMessage objResponse = new ResponseMessage();
            try
            {
                
                if (SeriesLimit > 0)
                {

                    if (divisible != 0)
                    {
                        if (position != 0)
                        {
                            int count = 1;
                            List<int> lstValues = new List<int>();
                            List<int> lstDivisibleValues = new List<int>();
                            int restult = 0;
                            for (int i = 0; i < SeriesLimit; i++)
                            {
                                lstValues.Add(SeriesModel.GetValues(i));
                            }
                            foreach (var x in lstValues)
                            {
                                if (x % divisible == 0)
                                    lstDivisibleValues.Add(x);
                            }
                            for (int index = 0; index < lstDivisibleValues.Count; index++)
                            {
                                if (count == position)
                                    restult = lstDivisibleValues[index];
                                count++;
                            }

                            objResponse.Result = restult;
                            objResponse.Series = lstValues;
                            objResponse.Status = HttpStatusCode.OK;
                            objResponse.Message = "Successful";
                        }
                        else
                        {
                            objResponse.Message = "Invalid position number";
                        }
                    }
                    else
                    {
                        objResponse.Message = "Invalid division number";
                    }
                    return objResponse;
                }

                else
                {
                    objResponse.Result = 0;
                    objResponse.Series = null;
                    objResponse.Status = HttpStatusCode.BadRequest;
                    objResponse.Message = ("Series number not valid");
                    return objResponse;
                }
            }

            catch
            {
                objResponse.Result = 0;
                objResponse.Series = null;
                objResponse.Status = HttpStatusCode.BadRequest;
                objResponse.Message = ("Series number not valid");
                return objResponse;
            }
        }
    }
}
