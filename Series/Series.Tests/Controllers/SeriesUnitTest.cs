﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Series.Models;
using Series.Controllers;


namespace Series.Tests.Controllers
{
    [TestClass]
    public class SeriesUnitTest
    {
        [TestMethod]
        public void TestMethod1()
        {
        }
        

       // [TestCase(20, 5, 3)]

        [TestMethod]
        public void SeriesDivisiblePositionN()
        {
            // Arrange
            int Series=20; int division=5; int position=3;
            SeriesAPIController controller = new SeriesAPIController();
            ResponseMessage objrestult = new ResponseMessage();
            // Act
            objrestult = controller.GetSeriesValue(Series, division, position);
            // Assert
            Assert.IsNotNull(objrestult.Series);
            Assert.AreEqual(355, objrestult.Result);
        }

        [TestMethod]
        public void SeriesDivisiblePositionN1()
        {
            // Arrange
            int Series = 20; int division = 5; int position = 2;
            SeriesAPIController controller = new SeriesAPIController();
            ResponseMessage objrestult = new ResponseMessage();
            // Act
            objrestult = controller.GetSeriesValue(Series, division, position);
            // Assert
            Assert.IsNotNull(objrestult.Series);
            Assert.AreEqual(105, objrestult.Result);
        }


        [TestMethod]
        public void SeriesDivisiblePositionN2()
        {
            // Arrange
            int Series = 20; int division = 5; int position = 1;
            SeriesAPIController controller = new SeriesAPIController();
            ResponseMessage objrestult = new ResponseMessage();
            // Act
            objrestult = controller.GetSeriesValue(Series, division, position);
            // Assert
            Assert.IsNotNull(objrestult.Series);
            Assert.AreEqual(5, objrestult.Result);
        }
    }
}
